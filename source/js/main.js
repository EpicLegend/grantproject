'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/slick.js
//= library/wow.js
//= library/jquery-ui.js


$(document).ready(function () {



	/* анимация блоков */
	new WOW().init({
		mobile: false
	});



	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");

			$("body").css("overflow", "hidden");
		} else {
			$(".navigation__content").removeClass("active");

			$("body").css("overflow", "auto");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");

			$("body").css("overflow", "auto");
		}
	});
	/* END откртие меню*/

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			$(".header").addClass("bg-dark");
		}  else {
			$(".header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */




	$(".video__content").on("click", function () {

	});
	$('#modalVideo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.data('src') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('iframe').attr("src", recipient);
	});
	$('#modalVideo').on('hidden.bs.modal', function (event) {
		var modal = $(this)
		modal.find('iframe').attr("src", '');
	});








	// slick slider
	$('.sssss').slick({
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
  		infinite: true,
 		speed: 300,
  		variableWidth: true,
  		autoplay: false,
  		autoplaySpeed: 3000,
  		edgeFriction: 3000,

	});

	$(".portfolio-main__slick .slick-dots").addClass("slick-dots_anim");

	var time = 2;
  var $bar,
      $slick,
      isPause,
      tick,
      percentTime;
  
  $slick = $('.portfolio-main__slick');
  $slick.slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	draggable: true,
	adaptiveHeight: false,
	dots: true,
	arrows: false,
	mobileFirst: true,
	pauseOnDotsHover: true,
  });
  
  $bar = $('.slider-progress .progress');
  
  $('.slider-wrapper').on({
    mouseenter: function() {
      isPause = true;
    },
    mouseleave: function() {
      isPause = false;
    }
  })
  
  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 10);
  }
  
  function interval() {
    if(isPause === false) {
      percentTime += 1 / (time+0.1);
      $bar.css({
        width: percentTime+"%"
      });
      if(percentTime >= 100)
        {
          $slick.slick('slickNext');
          startProgressbar();
        }
    }
  }
  
  
  function resetProgressbar() {
    $bar.css({
     width: 0+'%' 
    });
    clearTimeout(tick);
  }
  
  startProgressbar();


	$(".slider-build").each(function(index, element) {
		console.log(element);
		$(element).slick({
			arrows: true,
			dots: false,
			slidesToShow: 3,
			slidesToScroll: 1,
	  		infinite: true,
	  		prevArrow: '<div class="slider-build__arrow slider-build__arrow_left"><svg width="23" height="10" viewBox="0 0 23 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.4596 5.45962C22.7135 5.20578 22.7135 4.79422 22.4596 4.54038L18.323 0.403806C18.0692 0.149965 17.6576 0.149965 17.4038 0.403806C17.15 0.657647 17.15 1.0692 17.4038 1.32304L21.0808 5L17.4038 8.67696C17.15 8.9308 17.15 9.34235 17.4038 9.59619C17.6576 9.85003 18.0692 9.85003 18.323 9.59619L22.4596 5.45962ZM0 5.65H22V4.35H0V5.65Z" fill="#fff"/></svg></div>',
	  		nextArrow: '<div class="slider-build__arrow slider-build__arrow_right"><svg width="23" height="10" viewBox="0 0 23 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.4596 5.45962C22.7135 5.20578 22.7135 4.79422 22.4596 4.54038L18.323 0.403806C18.0692 0.149965 17.6576 0.149965 17.4038 0.403806C17.15 0.657647 17.15 1.0692 17.4038 1.32304L21.0808 5L17.4038 8.67696C17.15 8.9308 17.15 9.34235 17.4038 9.59619C17.6576 9.85003 18.0692 9.85003 18.323 9.59619L22.4596 5.45962ZM0 5.65H22V4.35H0V5.65Z" fill="#fff"/></svg></div>',
	  		responsive: [
			    {
			      breakpoint: 991,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			      }
			    },
		    ]
		});
	});
	$(".news-main__slider").each(function(index, element) {
		console.log(element);
		$(element).slick({
			arrows: false,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			centerMode: true,
  			variableWidth: true,
	  		infinite: true,
	  		responsive: [
			    {
			      breakpoint: 991,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			      }
			    },
		    ]
		});
	});

	$(".news-main .news-main__control_max").html( $(".news-main__slider .slick-slide:not(.slick-cloned)").length );

	$('.news-main__slider').on('afterChange', function(event, slick, direction) {

		var count = parseInt( $(".news-main__slider .slick-slide.slick-active").attr("data-slick-index") ) + 1;

		$(".news-main .news-main__control_first").html("");
		$(".news-main .news-main__control_first").html(count);
	// left
	});

	$(".news-main__right .slider-build__arrow_left").on("click", function () {
		$(".news-main__slider").slick("slickPrev");
	});
	$(".news-main__right .slider-build__arrow_right").on("click", function () {
		$(".news-main__slider").slick("slickNext");
	});
	

});


